module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true
  },
  extends: ['@nuxtjs/eslint-config-typescript', 'plugin:nuxt/recommended', 'plugin:prettier/recommended'],
  rules: {
    '@typescript-eslint/ban-ts-comment': 'off',
    'vue/multi-word-component-names': 'off',
    'no-console': ['error', { allow: ['warn', 'error'] }],
    'vue/no-template-key': 'error',
    'vue/no-multiple-template-root': 'off',
    'vue/no-v-for-template-key-on-child': 'error',
    'vue/component-tags-order': [
      'error',
      {
        order: ['script', 'template', 'style']
      }
    ]
  }
};
