export default defineNuxtConfig({
  css: ['~/assets/css/main.css'],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {}
    }
  },

  components: {
    global: true,
    dirs: ['~/components']
  },

  app: {
    head: {
      title: 'Kanye Rest',
      link: [{ rel: 'icon', type: 'image/x-icon', href: '/assets/favicon.ico' }],
      meta: [
        {
          name: 'viewport',
          content: 'width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0'
        }
      ]
    }
  },

  runtimeConfig: {
    public: {
      apiBaseURL: process.env.NUXT_API_BASE_URL
    }
  },

  typescript: {
    strict: true
  }
});
