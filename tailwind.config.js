module.exports = {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}'
  ],
  theme: {
    extend: {
      colors: {
        neutral: '#FBF8FA',
        dark: '#020202'
      },
      fontFamily: {
        mono: 'Martian Mono'
      },
      borderRadius: {
        '4xl': '40px'
      }
    }
  }
};
