import type { RuntimeConfig } from '@nuxt/schema';
import { UseFetchOptions } from 'nuxt/dist/app/composables';

export const fetchOptions = <T>(config: RuntimeConfig): UseFetchOptions<T> => {
  return {
    baseURL: config.public.apiBaseURL
  };
};
